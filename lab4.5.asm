.data
msg1: .asciiz "please enter an integer: "
msg2: .asciiz  "factorial of input number is: "
msg3: .asciiz  "input number is negative. Plesae try again.\n"

.text
.globl main
main:
	li $v0, 4
	la $a0, msg1
	syscall
	li $v0, 5
	syscall
	move $t0, $v0
	bgez $t0, next #if number >= 0
	li $v0, 4  #output error
	la $a0, msg3
	syscall
	j main

next:
	addi $sp, $sp, -4
	sw $ra, 4($sp)
	move $a0, $t0
	jal Factorial
	move $t0, $v0
	li $v0,4
	la $a0, msg2
	syscall
	li $v0,1 
	move $a0, $t0
	syscall
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra

Factorial:
	addi $sp, $sp, -4
	sw $ra, 4($sp)
	beqz $a0, terminate
	addi $sp, $sp, -4
	sw $a0, 4($sp)
	sub $a0, $a0, 1
	jal Factorial
	lw $t0, 4($sp)
	mul $v0, $v0, $t0
	lw $ra, 8($sp)
	addi $sp, $sp, 8
	jr $ra

terminate:
	li $v0, 1
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra
	

	