.data
msg1: .asciiz "the largest number is: "
msg2: .asciiz  "please enter a integer:  "
msg3: .asciiz  "please enter another integer:  " 

.text
.globl main
main:
	li $v0, 4
	la $a0, msg2
	syscall
	li $v0, 5  #read int
	syscall 
	move $t0, $v0
	li $v0, 4
	la $a0, msg3
	syscall
	li $v0, 5
	syscall
	move $t1, $v0

	addi $sp, $sp, -4
	sw $ra, 4($sp)
	addi $sp, $sp, -8
	sw $t0, 4($sp)
	sw $t1, 8($sp)
	jal Largest
	addi $sp, $sp, 8
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra

Largest:
	move $t0, $a0
	li $v0, 4
	la $a0, msg1
	syscall
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	slt $t0, $a0, $a1
	blez $t0, output
	move $a0 ,$a1

output:
	li $v0,1
	syscall
	jr $ra
