
.text
.globl main
main:
  addi $sp, $sp, -4  # substract 4 from stack pointer
  sw $ra, 4($sp)
  jal test    
  nop		
  lw $ra, 4($sp)
  addi $sp ,$sp, 4
  jr $ra

test:
    nop
    jr $ra